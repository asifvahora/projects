def buildApp() {
    echo "building app with Docker"
    sh "docker build . -t asifvahora/node-todo-app:latest"
}

def pushApp() {
    withCredentials([usernamePassword(credentialsId:'dockerhub credentials',passwordVariable: 'PASSWORD', usernameVariable: 'USERNAME')]){
               
                sh "echo $PASSWORD | docker login -u $USERNAME --password-stdin"
                sh "docker push asifvahora/node-todo-app:latest"
    }

}

def deployApp() {
    sh "docker-compose down && docker-compose up -d"
}

return this
