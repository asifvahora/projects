def buildJar() {
    echo "building the application..."
    sh 'mvn package'
} 

def buildImage() {
    echo "building the docker image..."
    withCredentials([usernamePassword(credentialsId:'dockerhub credentials',passwordVariable: 'PASSWORD', usernameVariable: 'USERNAME')]){
                sh "docker build -t asifvahora/java-maven:3.0 ."
                sh "echo $PASSWORD | docker login -u $USERNAME --password-stdin"
                sh "docker push asifvahora/java-maven:3.0"
    }

}

def deployApp() {
    sh "docker-compose down && docker-compose up -d"
}

return this
