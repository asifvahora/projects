import random
lst = ["Snake", "Water", "Gun"]
points_of_user = 0
points_of_comp = 0
attempts = 1
while(attempts<=10):
    comp = random.choice(lst).lower()
    user = input("Enter you choice\nSnake/Water/Gun: ").lower()
    print("comp's choice is:",comp)
    if comp == "snake" and user == "water":
        print("Snake has drunk water.")
        print("winner is comp!")
        points_of_comp+=1
    elif comp == "snake" and user == "gun":
        print("gun shot the Snake")
        print("winner is user!")
        points_of_user+=1
    elif comp == "water" and user == "gun":
        print("gun drowned into water")
        print("winner is comp!")
        points_of_comp += 1
    elif comp == "water" and user == "snake":
        print("Snake has drunk water.")
        print("winner is user!")
        points_of_user += 1
    elif comp == "gun" and user == u"water":
        print("Gun drowned into water")
        print("winner is user!")
        points_of_user += 1
    elif comp == "gun" and user == "snake":
        print("Gun shot the Snake")
        print("winner is comp!")
        points_of_comp += 1
    elif comp == "snake" and user == "snake":
        print("Match tied")
    elif comp == "gun" and user == "gun":
        print("Match tied")
    elif comp == "water" and user == "water":
        print("Match tied")
    else:
        print("Invalid input!")
    print("Points of user are: ", points_of_user)
    print("Points of comp are:", points_of_comp)
    print("You have", 10 - attempts, "attempts left")
    attempts += 1
else:
    print("Game over!")
    if points_of_comp > points_of_user:
        print("Tournament winner is comp with",points_of_comp,"points and \nloser is user with", points_of_user,"points")
    elif points_of_comp < points_of_user:
        print("Tournament winner is user with", points_of_user, "points and \nloser is comp with", points_of_comp,"points")
    else:
        print("Match tied with:\n user points:",points_of_user,"\ncomp points:",points_of_comp )





