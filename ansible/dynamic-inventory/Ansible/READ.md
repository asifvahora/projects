-   when you have servers that autoscale frequently, it will be frustrating to change host file all the time.
    to avoid that, we can use dynamic inventory, where we can pass on server dns name dynamically.
-   for aws, boto3 and botocore must be installed
-   i have created 3 ec2 servers using terraform
-   enable plugin in config.cfg (take the plugin name from ansible doc)
-   in ansible folder create inventory file which must end with "aws_ec2.yaml"
-   define plugin and region and run: ansible-inventory -i Ansible/inventory_aws_ec2.yaml --list....it will give you the whole list of the servers in that region
-   to just list servers --graph insted of --list

-   when you run above cmds it will display servers name as a group named aws_ec2....use that in your deployDocker.yaml in host field or user all
-   and for private_key and user add that inside ansible.cfg
-   execute playbook and now if you add or remove server, you don't need to update anything, it will work straight away


in terraform:
-   if public dns name is not there...inside vpc block enable_dns_hostnames=true as for private dns hostnames, you have to conect to the server from inside the vpc


-   if you want to differenciate between servers using tags or 
-   if you want to install docker on dev servers and nexus on prod servers in inventory file...
    use...keyed_groups:
            - key: tags (get 'tags' by running inventory with --list instead of groups)
              prefix: tag (you can add prefix in the beggining of a group name) 
            it will give you servers based on tags...use that tags as a host value (tag_Name_dev_1) in your playbook