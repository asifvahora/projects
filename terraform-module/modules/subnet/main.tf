resource "aws_subnet" "myapp-subnet1" {
  vpc_id     = var.vpc_id
  cidr_block = var.subnet_cidr_block

   tags = {
    Name: "${var.env_name}-subnet"
  }

 }


resource "aws_internet_gateway" "myapp-igw" {
    vpc_id = var.vpc_id
      tags = {
    Name: "${var.env_name}-igw"
  }
 }

 resource "aws_default_route_table" "myappmain-rtb"{
 default_route_table_id = var.default_route_table_id

   route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.myapp-igw.id
  }
    tags = {
    Name: "${var.env_name}-main-rtb"
  }
 }