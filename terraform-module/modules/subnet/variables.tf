variable "subnet_cidr_block" {
  description = "my app subnet"
}

variable "env_name" {
  description = "same name for all resources"
}

variable "vpc_id" {
    description = "vpc id for subnet"
}

variable "default_route_table_id" {
    description = "default route table id for subnet"
}