resource "aws_default_security_group" "myapp-default-sg" {
    vpc_id = var.vpc_id

    ingress {
      from_port = 22
      to_port = 22
      protocol = "tcp"
      cidr_blocks = [var.my_ip]
    }

    ingress {
      from_port = 8080
      to_port = 8080
      protocol = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }

    egress {
      from_port = 0
      to_port = 0
      protocol = "-1"
      cidr_blocks = ["0.0.0.0/0"]
      prefix_list_ids = []
    }
     tags = {
    Name: "${var.env_name}-default-sg"
  }
}


#EC2 server

data "aws_ami" "latest-amazon-linux-image" {
  most_recent = true 
  owners = ["amazon"]
  filter {
    name = "name"
    values = [var.image]
  }
  filter {
    name = "virtualization-type"
    values = ["hvm"]
  }
}

resource "aws_key_pair" "ssh-key" {
    key_name = "ec2server-key"
    public_key = file(var.ssh_public_key_location)
}

resource "aws_instance" "myapp-server" {
  ami = data.aws_ami.latest-amazon-linux-image.id
  instance_type = var.instance_type

  #to deploy it in our subnet otherwise it will be deployed in default subnet
  subnet_id = var.subnet_id
  vpc_security_group_ids = [aws_default_security_group.myapp-default-sg.id]

  #to ssh into the server
  associate_public_ip_address = true
  key_name = aws_key_pair.ssh-key.key_name

  #shell script for nginx docker image
  user_data = file("entry-script.sh")

  tags = {
    Name: "${var.env_name}-server"
  }
}