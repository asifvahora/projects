variable "vpc_id" {
    description = "vpc id which is in root main.tf"
}

variable "my_ip" {
    description = "ip address of your local machine"
}

variable "image" {
    description = "image of the vm"
}   

variable "ssh_public_key_location" {
    description = "ssh public key location"
}

variable "instance_type" {
    description = "vm instance tpye"
}

variable "subnet_id" {
    description = "vpc subnet id"
}

variable "env_name" {
    description = "same name for all resources"
}