#connect to provider
provider "aws" {
  region     = "eu-west-2"
 /*access_key = "AKIA5252W5KYHY64SAUO"
  secret_key = "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"
*/ }

#network setup
resource "aws_vpc" "myapp-vpc" {

  cidr_block = var.vpc_cidr_block

  tags = {
    Name: "${var.env_name}-vpc"
  }
}


#modules
module "myapp-subnet" {
  source = "./modules/subnet"
  subnet_cidr_block = var.subnet_cidr_block
  env_name = var.env_name
  vpc_id = aws_vpc.myapp-vpc.id
  default_route_table_id = aws_vpc.myapp-vpc.default_route_table_id
}

module "web-server" {
  source = "./modules/webserver"
  env_name = var.env_name
  vpc_id = aws_vpc.myapp-vpc.id
  my_ip = var.my_ip
  image = var.image
  ssh_public_key_location = var.ssh_public_key_location
  instance_type = var.instance_type
  subnet_id = module.myapp-subnet.subnet.id #to use one module into another module, we need export it in that modules output (subnet) and reference here
}






