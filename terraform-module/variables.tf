variable "vpc_cidr_block" {
  description = "my app vpc"
}

variable "subnet_cidr_block" {
  description = "my app subnet"
}

variable "env_name" {
  description = "same name for all resources"
}

variable "my_ip" {
  description = "ip of the local machine"
}

variable "instance_type" {
  description = "instance type"
}

variable "ssh_public_key_location" {
  description = "ssh public key location"
}


variable "image" {
  description = "vm image"
}