#connect to provider
provider "aws" {
  region     = "eu-west-2"
 /*access_key = "AKIA5252W5KYHY64SAUO"
  secret_key = "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"
*/ }

#variables
variable "vpc_cidr_block" {
  description = "my app vpc"
}

variable "subnet_cidr_block" {
  description = "my app subnet"
}

variable "env_name" {
  description = "same name for all resources"
}

variable "my_ip" {
  description = "ip of the local machine"
}

variable "instance_type" {
  description = "instance type"
}

variable "ssh_public_key_location" {
  description = "ssh public key location"
}

variable "image" {
    description = "image of vm"
}

#network setup
resource "aws_vpc" "myapp_vpc" {

  cidr_block = var.vpc_cidr_block

  tags = {
    Name: "${var.env_name}-vpc"
  }
}

resource "aws_subnet" "myapp_subnet" {
  vpc_id     = aws_vpc.myapp_vpc.id
  cidr_block = var.subnet_cidr_block

   tags = {
    Name: "${var.env_name}-subnet"
  }

 }

 resource "aws_internet_gateway" "myapp_igw" {
    vpc_id = aws_vpc.myapp_vpc.id
      tags = {
    Name: "${var.env_name}-igw"
  }
 }

 resource "aws_default_route_table" "myappmain-rtb"{
 default_route_table_id = aws_vpc.myapp_vpc.default_route_table_id

   route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.myapp_igw.id
  }
    tags = {
    Name: "${var.env_name}-main-rtb"
  }
 }

resource "aws_default_security_group" "myapp_default_sg" {
    vpc_id = aws_vpc.myapp_vpc.id

    ingress {
      from_port = 22
      to_port = 22
      protocol = "tcp"
      cidr_blocks = [var.my_ip]
    }

    ingress {
      from_port = 8080
      to_port = 8080
      protocol = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }

    egress {
      from_port = 0
      to_port = 0
      protocol = "-1"
      cidr_blocks = ["0.0.0.0/0"]
      prefix_list_ids = []
    }
     tags = {
    Name: "${var.env_name}-default-sg"
  }
}


#EC2 server

data "aws_ami" "latest-amazon-linux-image" {
  most_recent = true 
  owners = ["amazon"]
  filter {
    name = "name"
    values = ["${var.image}"]
  }
  filter {
    name = "virtualization-type"
    values = ["hvm"]
  }
}

resource "aws_key_pair" "ssh-key" {
    key_name = "instance-key"
    public_key = file(var.ssh_public_key_location)
}

resource "aws_instance" "myapp_server" {
  ami = data.aws_ami.latest-amazon-linux-image.id
  instance_type = var.instance_type

  count = 3

  #to deploy it in our subnet otherwise it will be deployed in default subnet
  subnet_id = aws_subnet.myapp_subnet.id
  vpc_security_group_ids = [aws_default_security_group.myapp_default_sg.id]

  #to ssh into the server
  associate_public_ip_address = true
  key_name = aws_key_pair.ssh-key.key_name

  tags = {
    Name: "${var.env_name}-${count.index + 1}"
  }
}

output  "ec2_ip" {
  value = aws_instance.myapp_server[*].public_ip
}
