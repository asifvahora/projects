def buildJar() {
    echo "Building jar"
  


}

def buildImage() {
    echo "Building docker image"
    withCredentials([usernamePassword(credentialsId: 'dockerhub credentials', passwordVariable: 'PASSWORD', usernameVariable: 'USERNAME')]) {
        sh "docker build --no-cache -t asifvahora/nodeapp ."
        sh "echo $PASSWORD | docker login -u $USERNAME --password-stdin"
        sh "docker push asifvahora/nodeapp "
}}


def deployImage() {
    sh "docker-compose down && docker-compose up -d"
}

return this
